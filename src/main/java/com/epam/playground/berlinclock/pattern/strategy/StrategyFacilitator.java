/**
 * 
 */
package com.epam.playground.berlinclock.pattern.strategy;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a simple BerlinClock strategies facilitator.
 * @author Angel_Arenas
 *
 */
public final class StrategyFacilitator
{
  private static final Map<Integer, BerlinClockStrategy> strategies = new HashMap<Integer, BerlinClockStrategy>();
  
  // I know I know, this should be externalized  :)
  static
  {
    strategies.put(Integer.valueOf(8), new ShortTimeStrategy());
    strategies.put(Integer.valueOf(11), new LongTimeStrategy());
    strategies.put(Integer.valueOf(18), new ExtraLargeTimeStrategy());
    // ... and so on, and so on, .....
  }
  
  /**
   * 
   */
  private StrategyFacilitator()
  {
  }

  // ---------------------------------------------------------------------------
  public static BerlinClockStrategy getStrategy(Integer timeLenght)
  {
    return strategies.get(timeLenght);
  }
}
