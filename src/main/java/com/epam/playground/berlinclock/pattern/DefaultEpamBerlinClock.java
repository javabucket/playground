/**
 * 
 */
package com.epam.playground.berlinclock.pattern;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.epam.playground.berlinclock.pattern.observer.GenericTimeObserver;
import com.epam.playground.berlinclock.pattern.observer.ShortTimeNoonObserver;
import com.epam.playground.berlinclock.pattern.observer.TimeObserver;
import com.epam.playground.berlinclock.pattern.strategy.BerlinClockStrategy;
import com.epam.playground.berlinclock.pattern.strategy.StrategyFacilitator;

/**
 * This is a default BerlinClock strategy selector.  Also is the second part of
 * a template pattern implementation. 
 * @author Angel_Arenas
 *
 */
public class DefaultEpamBerlinClock extends AbstractEpamBerlinClock
{
  private final Map<String, List<TimeObserver>> observers = new HashMap<String, List<TimeObserver>>();
  
  
  // ---------------------------------------------------------------------------
  private void addObserver(TimeObserver observer)
  {
    List<TimeObserver> list = observers.get(observer.getEventToObserve());
    if(null == list)
    {
      list = new ArrayList<TimeObserver>();
      observers.put(observer.getEventToObserve(), list);
    }
    list.add(observer);
  }
  
  /**
   * 
   */
  public DefaultEpamBerlinClock()
  {
    System.out.println("Starting default BerlinClock");
    
    ShortTimeNoonObserver stno = new ShortTimeNoonObserver();
    addObserver(stno);
    
    GenericTimeObserver gto1 = new GenericTimeObserver("12:00:00");
    addObserver(gto1);
    GenericTimeObserver gto2 = new GenericTimeObserver("08:27:13");
    addObserver(gto2);
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.AbstractEpamBerlinClock#getStrategy(java.lang.String)
   */
  @Override
  public BerlinClockStrategy getStrategy(String time)
  {// here we're just using a very simple aproach to select the wright strategy;
    // we're taking the lenght of the given time to determine the best strategy
    // to process the input
    BerlinClockStrategy strategy = StrategyFacilitator.getStrategy(Integer.valueOf(time.length()));
    
    return strategy;
  }

  // ---------------------------------------------------------------------------
  @Override
  public void notifyObservers(String time)
  {
    List<TimeObserver> list = observers.get(time);
    if(null == list || list.isEmpty())
    {
      return;
    }
    
    for (TimeObserver timeObserver : list)
    {
      timeObserver.update();
    }
  }

}
