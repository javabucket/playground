/**
 * 
 */
package com.epam.playground.berlinclock.pattern.strategy;

/**
 * This is the general contract for BerlinClock strategies
 * @author Angel_Arenas
 *
 */
public interface BerlinClockStrategy
{
  String getStrategyName();
  String[] buildIt(String time);
}
