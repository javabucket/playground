/**
 * 
 */
package com.epam.playground.berlinclock.pattern;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This is a cache for the hours and minutes of a BerlinClock
 * @author Angel_Arenas
 *
 */
public final class BerlinClockCache
{
  private static final Map<Integer, String> hours = new HashMap<Integer, String>(24);
  private static final Map<Integer, String> minutes = new HashMap<Integer, String>(60);

  static
  {
    // 1. let's get the hours first.
    for(int i = 0; i < 24; i++)
    {
      StringBuilder sb = new StringBuilder();
      sb.append(getTopHours(i));
      sb.append(getBottomHours(i));
      hours.put(Integer.valueOf(i), sb.toString());
    }
    
    // 2. now let's get the minutes loaded
    for(int i = 0; i < 60; i++)
    {
      StringBuilder sb = new StringBuilder();
      sb.append(getTopMinutes(i));
      sb.append(getBottomMinutes(i));
      minutes.put(Integer.valueOf(i), sb.toString());
    }
  }

  // ---------------------------------------------------------------------------
  private static String getTopHours(int number) 
  {
    return getOnOff(4, getTopNumberOfOnSigns(number));
  }

  // ---------------------------------------------------------------------------
  private static String getBottomHours(int number) 
  {
    return getOnOff(4, number % 5);
  }

  // ---------------------------------------------------------------------------
  private static String getTopMinutes(int number) 
  {
    return getOnOff(11, getTopNumberOfOnSigns(number), "Y").replaceAll("YYY", "YYR");
  }

  // ---------------------------------------------------------------------------
  private static String getBottomMinutes(int number) 
  {
      return getOnOff(4, number % 5, "Y");
  }

  // ---------------------------------------------------------------------------
  private static String getOnOff(int lamps, int onSigns) 
  {
    return getOnOff(lamps, onSigns, "R");
  }

  // ---------------------------------------------------------------------------
  private static String getOnOff(int lamps, int onSigns, String onSign) 
  {
    String out = "";
    for (int i = 0; i < onSigns; i++) 
    {
        out += onSign;
    }
    for (int i = 0; i < (lamps - onSigns); i++) 
    {
        out += "O";
    }
    return out;
  }
  
  // ---------------------------------------------------------------------------
  private static int getTopNumberOfOnSigns(int number) 
  {
    return (number - (number % 5)) / 5;
  }


  // ---------------------------------------------------------------------------
  public static String getHours(int hoursArg)
  {
    return hours.get(Integer.valueOf(hoursArg));
  }
  
  // ---------------------------------------------------------------------------
  public static String getMinutes(int minutesArg)
  {
    return minutes.get(Integer.valueOf(minutesArg));
  }
  
  // ---------------------------------------------------------------------------
  /**
   * 
   */
  private BerlinClockCache()
  {
    // TODO Auto-generated constructor stub
  }

}
