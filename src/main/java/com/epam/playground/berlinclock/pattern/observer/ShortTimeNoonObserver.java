/**
 * 
 */
package com.epam.playground.berlinclock.pattern.observer;

/**
 * @author Angel_Arenas
 *
 */
public class ShortTimeNoonObserver implements TimeObserver
{

  /**
   * 
   */
  public ShortTimeNoonObserver()
  {
    System.out.println("ShortTimeNoonObserver started.");
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.observer.TimeObserver#update()
   */
  @Override
  public void update()
  {
    System.out.println("OBSERVER: somebody requested a noon berlin date  :)");
  }

  @Override
  public String getEventToObserve()
  {
    return "12:00:00";
  }

}
