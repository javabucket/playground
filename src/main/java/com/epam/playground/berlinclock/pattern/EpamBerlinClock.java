package com.epam.playground.berlinclock.pattern;


/**
 * This is the interface that defines the general contract of the BerlinClock at
 * EPAM.
 * 
 * We're taking after the API introduced by the original BerlinClock, assuming
 * we cannot change it, so we have to follow thru.
 * 
 * @author Angel_Arenas
 *
 */
public interface EpamBerlinClock
{
  String[] convertToBerlinTime(String time);
}
