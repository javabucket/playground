/**
 * 
 */
package com.epam.playground.berlinclock.pattern.strategy;

import com.epam.playground.berlinclock.pattern.chain.ChainOfCommandFacilitator;

/**
 * This strategy deals with short format input time format. Ex. 13:25:16
 * @author Angel_Arenas
 *
 */
public class ShortTimeStrategy implements BerlinClockStrategy
{

  /**
   * 
   */
  public ShortTimeStrategy()
  {
    System.out.println("ShortTimeStrategy started.");
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.strategy.BerlinClockStrategy#getStrategyName()
   */
  @Override
  public String getStrategyName()
  {
    return ShortTimeStrategy.class.getName();
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.strategy.BerlinClockStrategy#buildIt(java.lang.String)
   */
  @Override
  public String[] buildIt(String time)
  {
    System.out.println("Starting execution of ShortTimeStrategy");
    String[] berlinTime = ChainOfCommandFacilitator.executeChain(ShortTimeStrategy.class, time);
    
    return berlinTime;
  }

}
