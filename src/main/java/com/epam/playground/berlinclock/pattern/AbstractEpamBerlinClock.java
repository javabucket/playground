/**
 * 
 */
package com.epam.playground.berlinclock.pattern;

import java.util.Arrays;

import com.epam.playground.berlinclock.pattern.strategy.BerlinClockStrategy;

/**
 * This an abstract implementation of {@link EpamBerlinClock} showing off the
 * usage of the template pattern.
 * @author Angel_Arenas
 *
 */
public abstract class AbstractEpamBerlinClock implements EpamBerlinClock
{

  /**
   * 
   */
  public AbstractEpamBerlinClock()
  {
    
  }

  // ---------------------------------------------------------------------------
  /**
   * This method provides the best strategy capable of building a Berlin 
   * representation of the given time.
   * @param time to be represented in Berlin format.
   * @return the strategy that will take care of building the Berlin 
   * representation of the given time.
   */
  public abstract BerlinClockStrategy getStrategy(String time);
  
  /**
   * This method invokes a to-be-implemented observer-pattern-method.
   * 
   * @param time
   */
  public abstract void notifyObservers(String time);
  // ---------------------------------------------------------------------------
  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.EpamBerlinClock#convertToBerlinTime(java.lang.String)
   */
  @Override
  public String[] convertToBerlinTime(String time)
  {
    System.out.println("just logging the input: " + time);

    notifyObservers(time);
    
    String[] result = null;
    BerlinClockStrategy strategy = getStrategy(time);
    System.out.println(strategy.getStrategyName() + " selected to do the work.");
    result = strategy.buildIt(time);
    
    // we're of assuming we won't blow up on a NPE here
    System.out.println("just logging the output: " + Arrays.asList(result));
    
    return result;
  }

}
