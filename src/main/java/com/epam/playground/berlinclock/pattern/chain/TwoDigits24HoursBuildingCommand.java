/**
 * 
 */
package com.epam.playground.berlinclock.pattern.chain;

import java.util.stream.Stream;

import com.epam.playground.berlinclock.pattern.BerlinClockCache;

/**
 * This command is on charge of bulding the hours belonging to the short time 
 * format BerlinClock.  The following is assumed:
 * </p>
 * <b>1.</b> time is separated by ":" 
 * </p>
 * <b>2.</b> everything can be parsed into int[]
 * </p>
 * <b>3.</b> hours input come in the first position
 * </p>
 * <b>4.</b> hours input come in 24h format.
 * @author Angel_Arenas
 *
 */
public class TwoDigits24HoursBuildingCommand implements TimeBuildingCommand
{

  /**
   * 
   */
  public TwoDigits24HoursBuildingCommand()
  {
    System.out.println("TwoDigits24HoursBuildingCommand started.");
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.chain.TimeBuildingCommand#execute(com.epam.playground.berlinclock.pattern.chain.TimeBuildingContext)
   */
  @Override
  public boolean execute(TimeBuildingContext ctx)
  {
    System.out.println("Starting execution of command TwoDigits24HoursBuildingCommand");
    BaseTimeBuildingContext context = (BaseTimeBuildingContext) ctx;
    Object parsedInput = context.getParsedInput();
    if(null == parsedInput)
    {
      String inputTime = context.getInputTime();
      parsedInput = Stream.of(inputTime.split(":")).mapToInt(Integer::parseInt).toArray();
      context.setParsedInput(parsedInput);
    }
    if(!(parsedInput instanceof int[]))
    {
      context.setOutputHours("ERROR");
      return true;
    }
    int[] parts = (int[]) parsedInput;
    String hours = BerlinClockCache.getHours(parts[0]);
    context.setOutputHours(hours);
    
    return false;
  }

}
