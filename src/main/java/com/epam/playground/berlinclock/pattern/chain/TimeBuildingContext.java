/**
 * 
 */
package com.epam.playground.berlinclock.pattern.chain;

/**
 * This is the general contract for the chain command context implementation
 * @author Angel_Arenas
 *
 */
public interface TimeBuildingContext
{
  Object getParameter(String name);
  void putParameter(String name, Object value);
  String getInputTime();
  void setParsedInput(Object parsed);
  Object getParsedInput();
}
