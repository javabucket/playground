/**
 * 
 */
package com.epam.playground.berlinclock.pattern.observer;

/**
 * @author Angel_Arenas
 *
 */
public class GenericTimeObserver implements TimeObserver
{
  private String observableTimeEvent;
  /**
   * 
   */
  public GenericTimeObserver(String timeToObserve)
  {
    observableTimeEvent = timeToObserve;
    System.out.println("GenericTimeObserver started for [" + timeToObserve + "]");
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.observer.TimeObserver#getEventToObserve()
   */
  @Override
  public String getEventToObserve()
  {
    return observableTimeEvent;
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.observer.TimeObserver#update()
   */
  @Override
  public void update()
  {
    System.out.println("OBSERVER: somebody requested the time [" + observableTimeEvent + "]");
  }

}
