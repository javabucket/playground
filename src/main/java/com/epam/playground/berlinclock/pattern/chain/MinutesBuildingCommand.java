/**
 * 
 */
package com.epam.playground.berlinclock.pattern.chain;

import java.util.stream.Stream;

import com.epam.playground.berlinclock.pattern.BerlinClockCache;

/**
 * This command builds the minutes representation of the BerlinClock.  This 
 * command also assumes the following:
 * </p>
 * <b>1.</b> time is separated by ":" 
 * </p>
 * <b>2.</b> everything can be parsed into int[]
 * </p>
 * <b>3.</b> minutes input comes in the second position

 * @author Angel_Arenas
 *
 */
public class MinutesBuildingCommand implements TimeBuildingCommand
{

  /**
   * 
   */
  public MinutesBuildingCommand()
  {
    System.out.println("MinutesBuildingCommand started.");
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.chain.TimeBuildingCommand#execute(com.epam.playground.berlinclock.pattern.chain.TimeBuildingContext)
   */
  @Override
  public boolean execute(TimeBuildingContext ctx)
  {
    System.out.println("Starting execution of command MinutesBuildingCommand");
    BaseTimeBuildingContext context = (BaseTimeBuildingContext) ctx;
    Object parsedInput = context.getParsedInput();
    if(null == parsedInput)
    {
      String inputTime = context.getInputTime();
      parsedInput = Stream.of(inputTime.split(":")).mapToInt(Integer::parseInt).toArray();
      context.setParsedInput(parsedInput);
    }
    if(!(parsedInput instanceof int[]))
    {
      context.setOutputMinutes("ERROR");
      return true;
    }
    int[] parts = (int[]) parsedInput;
    String hours = BerlinClockCache.getMinutes(parts[1]);
    context.setOutputMinutes(hours);
    
    return false;
  }

}
