/**
 * 
 */
package com.epam.playground.berlinclock.pattern.strategy;

/**
 * @author Angel_Arenas
 *
 */
public class ExtraLargeTimeStrategy implements BerlinClockStrategy
{

  /**
   * 
   */
  public ExtraLargeTimeStrategy()
  {
    System.out.println("ExtraLargeTimeStrategy started.");
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.strategy.BerlinClockStrategy#getStrategyName()
   */
  @Override
  public String getStrategyName()
  {
    return ExtraLargeTimeStrategy.class.getName();
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.strategy.BerlinClockStrategy#buildIt(java.lang.String)
   */
  @Override
  public String[] buildIt(String time)
  {
    // TODO Auto-generated method stub
    return null;
  }

}
