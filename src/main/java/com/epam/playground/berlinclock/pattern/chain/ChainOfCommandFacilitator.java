/**
 * 
 */
package com.epam.playground.berlinclock.pattern.chain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.epam.playground.berlinclock.pattern.strategy.BerlinClockStrategy;
import com.epam.playground.berlinclock.pattern.strategy.ShortTimeStrategy;

/**
 * This is a simple facilitator that replaces a configuration file for the 
 * chain of command implementation.
 * @author Angel_Arenas
 *
 */
public final class ChainOfCommandFacilitator
{
  // the firs map parameter is the name of the chain, and the second is the commands 
  // belonging to that command
  private static final Map<String,List<TimeBuildingCommand>> chain = new HashMap<String,List<TimeBuildingCommand>>();
  
  // this whole thing is replacing an otherwise file configuration
  static
  {
    // set the commands for short time format
    List<TimeBuildingCommand> shortTimeCommands = new ArrayList<TimeBuildingCommand>();
    shortTimeCommands.add(new SecondsBuildingCommand());
    shortTimeCommands.add(new TwoDigits24HoursBuildingCommand());
    shortTimeCommands.add(new MinutesBuildingCommand());
    chain.put(ShortTimeStrategy.class.getName(), shortTimeCommands);
    
    // ... and so on, and so on, chain by chain,...
  }
  /**
   * 
   */
  private ChainOfCommandFacilitator()
  {
  }

  // ---------------------------------------------------------------------------
  public static String[] executeChain(Class<?> chainOwner, String time)
  {
    List<TimeBuildingCommand> commands = chain.get(chainOwner.getName());
    if(null == commands || commands.isEmpty())
    {
      System.out.println("No chain found for this strategy: " + chainOwner.getName());
      return new String[] {"ERROR-NO CHAIN"};
    }
    
    BaseTimeBuildingContext context = new BaseTimeBuildingContext(time);
    
    for (TimeBuildingCommand timeBuildingCommand : commands)
    {
      boolean outcome = timeBuildingCommand.execute(context);
      if(outcome)
      {
        return context.getOutputTime();
      }
    }
    
    return context.getOutputTime();
  }
  
}
