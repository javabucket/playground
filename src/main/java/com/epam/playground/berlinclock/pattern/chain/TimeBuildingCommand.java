/**
 * 
 */
package com.epam.playground.berlinclock.pattern.chain;

/**
 * This is the Command interface for all the explicit commands to implement.  
 * Each command is intended to build a specific part of the BerlinClock 
 * representation.
 * @author Angel_Arenas
 *
 */
public interface TimeBuildingCommand
{
  /**
   * It processes a certain portion of the given time
   * @param ctx
   * @return <b>false</b> if the process should continue on to the next command
   */
  boolean execute(TimeBuildingContext ctx);
}
