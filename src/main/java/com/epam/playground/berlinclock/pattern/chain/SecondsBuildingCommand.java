package com.epam.playground.berlinclock.pattern.chain;

import java.util.stream.Stream;

/**
 * This command builds the seconds representation of the BerlinClock.  This 
 * command also assumes the following:
 * </p>
 * <b>1.</b> time is separated by ":" 
 * </p>
 * <b>2.</b> everything can be parsed into int[]
 * </p>
 * <b>3.</b> seconds input comes in the third position
 * @author Angel_Arenas
 *
 */
public class SecondsBuildingCommand implements TimeBuildingCommand
{

  public SecondsBuildingCommand()
  {
    System.out.println("SecondsBuildingCommand started.");
  }

  @Override
  public boolean execute(TimeBuildingContext ctx)
  {
    System.out.println("Starting execution of command SecondsBuildingCommand");
    BaseTimeBuildingContext context = (BaseTimeBuildingContext) ctx;
    Object parsedInput = context.getParsedInput();
    if(null == parsedInput)
    {
      String inputTime = context.getInputTime();
      parsedInput = Stream.of(inputTime.split(":")).mapToInt(Integer::parseInt).toArray();
      context.setParsedInput(parsedInput);
    }
    if(!(parsedInput instanceof int[]))
    {
      context.setOutputSeconds("ERROR");
      return true;
    }
    
    int[] parts = (int[]) parsedInput;
    if (parts[2] % 2 == 0) context.setOutputSeconds("Y");
    else context.setOutputSeconds("O");

    return false;
  }

}
