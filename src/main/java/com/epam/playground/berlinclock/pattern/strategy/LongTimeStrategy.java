/**
 * 
 */
package com.epam.playground.berlinclock.pattern.strategy;

/**
 * @author Angel_Arenas
 *
 */
public class LongTimeStrategy implements BerlinClockStrategy
{

  /**
   * 
   */
  public LongTimeStrategy()
  {
    System.out.println("LongTimeStrategy started.");
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.strategy.BerlinClockStrategy#getStrategyName()
   */
  @Override
  public String getStrategyName()
  {
    return LongTimeStrategy.class.getName();
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.strategy.BerlinClockStrategy#buildIt(java.lang.String)
   */
  @Override
  public String[] buildIt(String time)
  {
    System.out.println("Starting execution of LongTimeStrategy");
    String[] result = {"dummy","date","here"};
    return result;
  }

}
