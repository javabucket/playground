/**
 * 
 */
package com.epam.playground.berlinclock.pattern.chain;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Angel_Arenas
 *
 */
public class BaseTimeBuildingContext implements TimeBuildingContext
{
  private final String inputTime;
  private final String[] outputTime = new String[3];
  private Object parsedInput;
  private Map<String, Object> parameters;
  
  
  public BaseTimeBuildingContext(String inputTime)
  {
    super();
    this.inputTime = inputTime;
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.chain.TimeBuildingContext#getParameter(java.lang.String)
   */
  @Override
  public Object getParameter(String name)
  {
    if(null == parameters)
    {
      return null;
    }
    
    return parameters.get(name);
  }

  /* (non-Javadoc)
   * @see com.epam.playground.berlinclock.pattern.chain.TimeBuildingContext#putParameter(java.lang.String, java.lang.Object)
   */
  @Override
  public void putParameter(String name, Object value)
  { // if this were a multithreaded environment probably would've needed to 
    // synchronize the whole thing over
    if(null == parameters)
    {
      parameters = new HashMap<String, Object>();
    }
    parameters.put(name, value);
  }

  // ---------------------------------------------------------------------------
  @Override
  public String getInputTime()
  {
    return inputTime;
  }

  // ---------------------------------------------------------------------------
  public String[] getOutputTime()
  {
    return outputTime;
  }

  // ---------------------------------------------------------------------------
  public void setOutputSeconds(String seconds)
  {
    // we're assuming everything is clean here, so there's no need for checking
    outputTime[0] = seconds;
  }
  
  public void setOutputHours(String hours)
  {
    // we're assuming everything is clean here, so there's no need for checking
    outputTime[1] = hours;
    
  }
  
  public void setOutputMinutes(String minutes)
  {
    // we're assuming everything is clean here, so there's no need for checking
    outputTime[2] = minutes;
  }

  // ---------------------------------------------------------------------------
  @Override
  public void setParsedInput(Object parsed)
  {
    // we'll just skip defensive coding here since it's just a patterns drill
    parsedInput = parsed;
  }

  @Override
  public Object getParsedInput()
  {
    return parsedInput;
  }
  
}
