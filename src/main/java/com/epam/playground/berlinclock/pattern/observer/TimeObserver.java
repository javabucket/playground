/**
 * 
 */
package com.epam.playground.berlinclock.pattern.observer;

/**
 * This is the time observer interface part of the observer pattern
 * @author Angel_Arenas
 *
 */
public interface TimeObserver
{
  String getEventToObserve();
  void update();
}
