/**
 * 
 */
package com.epam.playground.berlinclock.pattern;

/**
 * @author Angel_Arenas
 *
 */
public final class BerlinClockDriver
{

  /**
   * 
   */
  private BerlinClockDriver()
  {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param args
   */
  public static void main(String[] args)
  {
    DefaultEpamBerlinClock driver = new DefaultEpamBerlinClock();
    driver.convertToBerlinTime("13:45:17");
    System.out.println("-----------------------------------------------------");
    driver.convertToBerlinTime("13:45:18");
    System.out.println("-----------------------------------------------------");
    driver.convertToBerlinTime("13:45:19");
    System.out.println("-----------------------------------------------------");
    driver.convertToBerlinTime("22:00:32");
    System.out.println("-----------------------------------------------------");
    driver.convertToBerlinTime("10:00:32 PM");
    System.out.println("-----------------------------------------------------");
    driver.convertToBerlinTime("12:00:00");
    System.out.println("-----------------------------------------------------");
    driver.convertToBerlinTime("12:00:01");
    System.out.println("-----------------------------------------------------");
    driver.convertToBerlinTime("08:27:13");
    System.out.println("-----------------------------------------------------");
  }

}
