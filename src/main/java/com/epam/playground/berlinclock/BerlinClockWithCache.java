/**
 * 
 */
package com.epam.playground.berlinclock;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This ios an implementation of the BerlinClock using caching for pre
 * calculating values and caching them so they are available for consuption.
 * 
 * @author Angel_Arenas
 *
 */
public class BerlinClockWithCache
{
  private static final Map<Integer, String> hours = new HashMap<Integer, String>(24);
  private static final Map<Integer, String> minutes = new HashMap<Integer, String>(60);

  static
  {
    // 1. let's get the hours first.
    for(int i = 0; i < 24; i++)
    {
      StringBuilder sb = new StringBuilder();
      sb.append(getTopHours(i));
      sb.append(getBottomHours(i));
      hours.put(Integer.valueOf(i), sb.toString());
    }
    
    // 2. now let's get the minutes loaded
    for(int i = 0; i < 60; i++)
    {
      StringBuilder sb = new StringBuilder();
      sb.append(getTopMinutes(i));
      sb.append(getBottomMinutes(i));
      minutes.put(Integer.valueOf(i), sb.toString());
    }
  }

  // ---------------------------------------------------------------------------
  public static String[] convertToBerlinTime(String time)
  {
    int[] parts = Stream.of(time.split(":")).mapToInt(Integer::parseInt).toArray();
    return new String[] { 
        getSeconds(parts[2]),
        hours.get(Integer.valueOf(parts[0])),
        minutes.get(Integer.valueOf(parts[1]))};
  }

  // ---------------------------------------------------------------------------
  private static String getSeconds(int number) 
  {
    if (number % 2 == 0) return "Y";
    else return "O";
  }

  // ---------------------------------------------------------------------------
  private static String getTopHours(int number) 
  {
    return getOnOff(4, getTopNumberOfOnSigns(number));
  }

  // ---------------------------------------------------------------------------
  private static String getBottomHours(int number) 
  {
    return getOnOff(4, number % 5);
  }

  // ---------------------------------------------------------------------------
  private static String getTopMinutes(int number) 
  {
    return getOnOff(11, getTopNumberOfOnSigns(number), "Y").replaceAll("YYY", "YYR");
  }

  // ---------------------------------------------------------------------------
  private static String getBottomMinutes(int number) 
  {
      return getOnOff(4, number % 5, "Y");
  }

  // ---------------------------------------------------------------------------
  private static String getOnOff(int lamps, int onSigns) 
  {
    return getOnOff(lamps, onSigns, "R");
  }

  // ---------------------------------------------------------------------------
  private static String getOnOff(int lamps, int onSigns, String onSign) 
  {
    String out = "";
    for (int i = 0; i < onSigns; i++) 
    {
        out += onSign;
    }
    for (int i = 0; i < (lamps - onSigns); i++) 
    {
        out += "O";
    }
    return out;
  }
  
  // ---------------------------------------------------------------------------
  private static int getTopNumberOfOnSigns(int number) 
  {
    return (number - (number % 5)) / 5;
  }


  // ---------------------------------------------------------------------------
  public static void main(String[] args)
  {
    String[] berlinTime = convertToBerlinTime("13:17:16");
    System.out.println(Arrays.asList(berlinTime));
    
    berlinTime = convertToBerlinTime("23:45:13");
    System.out.println(Arrays.asList(berlinTime));
    
  }
}
